﻿
var id = null;
var server = null;

started();
loadIdClientFromStorage();

function started() {

    console.log("loadedBackgroundJS");

    chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
        if (request.data["type"] === "REST") {
            RestSendIPAddress(request.data["data"]);
        }
        else {
            console.log("MessageForBackgroundJS");
            console.log(request.data);
            sendResponse("ReturnedDataFromBackgroundWorker");
        }
    });
}


function RestSendIPAddress(data) {
    var url = "http://" + server + "/"
    $.ajax({
        url: url,
        type: "put",
        dataType: "json",
        data: { pingHostName:data, clientID:id},
        success: function (data) { console.log(data); }
    });
}

function loadIdClientFromStorage() {
    chrome.storage.local.get('idClient', function (result) {
        console.log(result['idClient']);
        id = result['idClient'];
        loadIpServerFromStorage();
    });
}

function loadIpServerFromStorage() {
    chrome.storage.local.get('ipServer', function (result) {
        console.log(result['ipServer']);
        server = result['ipServer'];
    });
}