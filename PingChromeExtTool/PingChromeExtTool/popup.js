var id = null;
var server = null;

$(function () { // after load html
    firstLoad();
});

function testConnect() {
    //wysy�anie danych do aktywnej karty
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { data: "hello" }, null, function (response) {
            console.log("ResponseFromActiveTab:"+ response);
        });
    });

    //Wysy�anie danych do t�a
    chrome.runtime.sendMessage({ data: "longData" }, null, function (response) {
        console.log("ResponseFromBackground:" + response);
    });
}

function readTabsAmounts(tabs) {
    var tabsCount = tabs.length;
    var regex = RegExp('^https?:\/\/');
    for (var i = 0; i < tabsCount; i++) {
        if (regex.test(tabs[i].url)) {//pomijanie dzia�a� na karcie je�li jest to karta wewn�trzna od chroma
            chrome.tabs.sendMessage(tabs[i].id, { data: tabsCount + "I'm tab with number " + i + " and id " + tabs[i].id }, null, function (response) {
                console.log("ResponseFromActiveTab:" + response);
            });
        }
    }
}

function sendRestData() {
    var pingAddres = $("#pingAddrInput")[0].value;
    console.log(pingAddres);
    chrome.runtime.sendMessage({
        data: { type: "REST", data: pingAddres} }, null, function (response) {
        console.log("ResponseFromBackground:" + response);
    });
}

document.addEventListener('DOMContentLoaded', function () {

    document.querySelector('#sendData').addEventListener(
        'click', sendRestData);
    document.querySelector('#settingsButton').addEventListener(
        'click', showSettings);
    document.querySelector('#saveButton').addEventListener(
        'click', saveAndHideSettings);

  //chrome.tabs.query({}, readTabsAmounts);
});

function firstLoad() {
    loadDataFromStorage();
}

function showSettings() {
    $(".settings").css("display", "block");
    $(".mainElements").css("display", "none");
}

function hideSettings() {
    $(".settings").css("display", "none");
    $(".mainElements").css("display", "block");
}

function saveConnectionData() {
    var idClient = $("#idInput")[0].value;
    var ipServer = $("#ipInput")[0].value;
    chrome.storage.local.set({ "idClient": idClient, "ipServer": ipServer }, loadDataFromStorage);
}

function loadDataFromStorage() {
    loadIdClientFromStorage();
    hideSettings();
}

function loadIdClientFromStorage() {
    chrome.storage.local.get('idClient', function (result) {
        console.log(result['idClient']);
        id = result['idClient'];
        loadIpServerFromStorage();
    });
}

function loadIpServerFromStorage() {
    chrome.storage.local.get('ipServer', function (result) {
        console.log(result['ipServer']);
        server = result['ipServer'];
        setDataInput();
    });
}
function saveAndHideSettings() {
    hideSettings();
    saveConnectionData();
}

function setDataInput() {
    console.log("test");
    $("#idInput")[0].value = id;
    $("#ipInput")[0].value = server;
}