﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PingClientTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;

namespace PingClientTool.TCP
{
    public class ServerTCPForTest
    {
        TcpListener serverTCP;
        byte[] buffer = new Byte[512];
        public int port = 5050;
        public string ip = "127.0.0.1";
        public int clientID = 1337;
        public ServerTCPForTest()
        {
            int port = 5050;
            IPAddress ipParsed = IPAddress.Parse(ip);
            serverTCP = new TcpListener(ipParsed, port);
            serverTCP.Start();
        }
        ~ServerTCPForTest() // destruktor nie uruchamia się po zakońćzeniu TestMethod tylko na samym końcu testowania wszystkie, a konsruktor uruhcamia się dla każdego TestMethod.
        {
            serverTCP.Stop();
        }
    }

    [TestClass()]
    public class PingToolTests
    {
        [TestMethod()]
        public void TcpConnect()
        {
            ServerTCPForTest test = new ServerTCPForTest();
            TcpConnection tcpClient = new TcpConnection(test.ip, test.port, test.clientID, PrintText);
            try
            {
                tcpClient.Start();
            }
            catch
            {
                Assert.Fail();
            }
            finally
            {
                tcpClient.Close();
            }
        }

        //For testing and debuging.
        static void PrintText(string message)
        {
            Debug.WriteLine(message);
            Debug.WriteLine("");
        }


        [TestMethod()]
        public void TcpReceiveData()
        {
            ServerTCPForTest test = new ServerTCPForTest();
            TcpConnection tcpClient = new TcpConnection(test.ip, test.port, test.clientID, PrintText);
            tcpClient.Start();
            tcpClient.Close();
        }

        [TestMethod()]
        public void TcpReceiveData2()
        {
            ServerTCPForTest test = new ServerTCPForTest();
            TcpConnection tcpClient = new TcpConnection(test.ip, test.port, test.clientID, PrintText);
            tcpClient.Start();
            tcpClient.Close();
        }

    }
}
namespace PingClientTool.Buffer
{
    [TestClass()]
    public class TestBuffer
    {
        [TestMethod()]
        public void AddingMoreThanFull()
        {
            Buffer<int> TestBuffer = new Buffer<int>(5);
            for (int i = 0; i < 50; i++)
            {
                TestBuffer.Add(i);
            }
        }

        [TestMethod()]
        public void TestMaxElements()
        {
            Random rand = new Random();
            Buffer<int> TestBuffer = new Buffer<int>(short.MaxValue);
            for (int i = 0; i < short.MaxValue*2; i++)
            {
                TestBuffer.Add(rand.Next());
            }
        }

        [DataTestMethod]
        [DataRow(8, 30, 5, 27)]
        [DataRow(8, 30, 8, 22)]
        [DataRow(10, 30, 5, 25)]
        [DataRow(10, 5, 4, 4)]
        public void ReadElement(int maxElements, int loopAmount,int testIndex,int result)
        {
            Buffer<int> TestBuffer = new Buffer<int>((short)maxElements);
            for (int i = 0; i < loopAmount; i++)
            {
                TestBuffer.Add(i);
            }
            Assert.AreEqual(result, TestBuffer.Get((short)testIndex));
        }

        [TestMethod()]
        public void OutOfRangeOverMax()
        {
            Buffer<int> TestBuffer = new Buffer<int>(5);
            for (int i = 0; i < 10; i++)
            {
                TestBuffer.Add(i);
            }
            try
            {
                TestBuffer.Get(5);
                Assert.Fail();
            }
            catch (IndexOutOfRangeException) { }//Poprawny wyjątek
            catch { Assert.Fail(); }
        }
    }
}