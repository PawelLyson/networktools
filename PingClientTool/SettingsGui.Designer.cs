﻿namespace PingClientTool
{
    partial class SettingsGui
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ServerSettingsGroupBox = new System.Windows.Forms.GroupBox();
            this.ServerIDTextBox = new System.Windows.Forms.TextBox();
            this.ServerIDLabel = new System.Windows.Forms.Label();
            this.ServerPortTextBox = new System.Windows.Forms.TextBox();
            this.ServerPortLabel = new System.Windows.Forms.Label();
            this.ServerIPTextBox = new System.Windows.Forms.TextBox();
            this.ServerIPLabel = new System.Windows.Forms.Label();
            this.SaveAndQuitButton = new System.Windows.Forms.Button();
            this.ServerSettingsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // ServerSettingsGroupBox
            // 
            this.ServerSettingsGroupBox.Controls.Add(this.ServerIDTextBox);
            this.ServerSettingsGroupBox.Controls.Add(this.ServerIDLabel);
            this.ServerSettingsGroupBox.Controls.Add(this.ServerPortTextBox);
            this.ServerSettingsGroupBox.Controls.Add(this.ServerPortLabel);
            this.ServerSettingsGroupBox.Controls.Add(this.ServerIPTextBox);
            this.ServerSettingsGroupBox.Controls.Add(this.ServerIPLabel);
            this.ServerSettingsGroupBox.Location = new System.Drawing.Point(12, 12);
            this.ServerSettingsGroupBox.Name = "ServerSettingsGroupBox";
            this.ServerSettingsGroupBox.Size = new System.Drawing.Size(192, 101);
            this.ServerSettingsGroupBox.TabIndex = 0;
            this.ServerSettingsGroupBox.TabStop = false;
            this.ServerSettingsGroupBox.Text = "Ustawienia";
            // 
            // ServerIDTextBox
            // 
            this.ServerIDTextBox.Location = new System.Drawing.Point(75, 68);
            this.ServerIDTextBox.Name = "ServerIDTextBox";
            this.ServerIDTextBox.Size = new System.Drawing.Size(100, 20);
            this.ServerIDTextBox.TabIndex = 5;
            // 
            // ServerIDLabel
            // 
            this.ServerIDLabel.AutoSize = true;
            this.ServerIDLabel.Location = new System.Drawing.Point(6, 71);
            this.ServerIDLabel.Name = "ServerIDLabel";
            this.ServerIDLabel.Size = new System.Drawing.Size(65, 13);
            this.ServerIDLabel.TabIndex = 4;
            this.ServerIDLabel.Text = "Identyfikator";
            // 
            // ServerPortTextBox
            // 
            this.ServerPortTextBox.Location = new System.Drawing.Point(75, 42);
            this.ServerPortTextBox.Name = "ServerPortTextBox";
            this.ServerPortTextBox.Size = new System.Drawing.Size(100, 20);
            this.ServerPortTextBox.TabIndex = 3;
            // 
            // ServerPortLabel
            // 
            this.ServerPortLabel.AutoSize = true;
            this.ServerPortLabel.Location = new System.Drawing.Point(6, 45);
            this.ServerPortLabel.Name = "ServerPortLabel";
            this.ServerPortLabel.Size = new System.Drawing.Size(62, 13);
            this.ServerPortLabel.TabIndex = 2;
            this.ServerPortLabel.Text = "Serwer Port";
            // 
            // ServerIPTextBox
            // 
            this.ServerIPTextBox.Location = new System.Drawing.Point(75, 16);
            this.ServerIPTextBox.Name = "ServerIPTextBox";
            this.ServerIPTextBox.Size = new System.Drawing.Size(100, 20);
            this.ServerIPTextBox.TabIndex = 1;
            // 
            // ServerIPLabel
            // 
            this.ServerIPLabel.AutoSize = true;
            this.ServerIPLabel.Location = new System.Drawing.Point(6, 19);
            this.ServerIPLabel.Name = "ServerIPLabel";
            this.ServerIPLabel.Size = new System.Drawing.Size(53, 13);
            this.ServerIPLabel.TabIndex = 0;
            this.ServerIPLabel.Text = "Serwer IP";
            // 
            // SaveAndQuitButton
            // 
            this.SaveAndQuitButton.Location = new System.Drawing.Point(53, 119);
            this.SaveAndQuitButton.Name = "SaveAndQuitButton";
            this.SaveAndQuitButton.Size = new System.Drawing.Size(97, 24);
            this.SaveAndQuitButton.TabIndex = 7;
            this.SaveAndQuitButton.Text = "Zapisz i zamknij";
            this.SaveAndQuitButton.UseVisualStyleBackColor = true;
            this.SaveAndQuitButton.Click += new System.EventHandler(this.SaveAndQuitButton_Click);
            // 
            // SettingsGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(221, 151);
            this.Controls.Add(this.SaveAndQuitButton);
            this.Controls.Add(this.ServerSettingsGroupBox);
            this.Name = "SettingsGui";
            this.Text = "Ustawienia";
            this.Shown += new System.EventHandler(this.SettingsGui_Shown);
            this.ServerSettingsGroupBox.ResumeLayout(false);
            this.ServerSettingsGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox ServerSettingsGroupBox;
        private System.Windows.Forms.Label ServerIPLabel;
        private System.Windows.Forms.TextBox ServerIDTextBox;
        private System.Windows.Forms.Label ServerIDLabel;
        private System.Windows.Forms.TextBox ServerPortTextBox;
        private System.Windows.Forms.Label ServerPortLabel;
        private System.Windows.Forms.TextBox ServerIPTextBox;
        private System.Windows.Forms.Button SaveAndQuitButton;
    }
}