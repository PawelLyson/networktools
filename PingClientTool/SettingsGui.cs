﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace PingClientTool
{
    public partial class SettingsGui : Form
    {
        Configurator<ConfigData> PreparedConfigurator { get; set; }
        ConfigData Config2;
        public SettingsGui()
        {
            InitializeComponent();
            //PreparedConfigurator = new Configurator<ConfigData>("config2.dat", ref Config2);
        }

        private void LoadDataFromConfig()
        {
            PreparedConfigurator = new Configurator<ConfigData>("config.dat", ref Config2);

            ServerIPTextBox.Invoke((MethodInvoker)delegate
            {
                ServerIPTextBox.Text = Config2.serverIP;
            });
            ServerPortTextBox.Invoke((MethodInvoker)delegate
            {
                ServerPortTextBox.Text = Config2.serverPort.ToString();
            });
            ServerIDTextBox.Invoke((MethodInvoker)delegate
            {
                ServerIDTextBox.Text = Config2.clientID.ToString();
            });
        }

        private void SaveDataToConfig()
        {
            Config2.serverIP = ServerIPTextBox.Text;
            Config2.serverPort = int.Parse(ServerPortTextBox.Text);
            Config2.clientID = int.Parse(ServerIDTextBox.Text);
            PreparedConfigurator.SaveConfig();
            Debug.WriteLine("Saving Config");
        }

        private void SettingsGui_Shown(object sender, EventArgs e)
        {
            LoadDataFromConfig();
            Debug.WriteLine("Loading Config");
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
        }

        private void SaveAndQuitButton_Click(object sender, EventArgs e)
        {
            SaveDataToConfig();
            Close();
        }
    }
}
