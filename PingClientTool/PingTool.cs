﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using System.Runtime.Remoting.Messaging;
using System.IO;
using Newtonsoft.Json;
using System.Collections;

namespace PingClientTool
{
    public class PingTool
    {
        private ConfigData Config = new ConfigData();
        private Configurator<ConfigData> PreparedConfigurator;
        private TcpConnection TcpClient;
        private Action<PingObject> CallbackMethodPing;
        public bool isConnected
        {
            get
            {
                return TcpClient.IsConnected;
            }
        }

        public PingTool(Action<PingObject> callbackPing)
        {
            PreparedConfigurator = new Configurator<ConfigData>("config.dat", ref Config);
            CallbackMethodPing = callbackPing;
            //preparedConfigurator.SaveConfig();
            TcpClient = new TcpConnection(Config.serverIP, Config.serverPort, Config.clientID, RouteTCPMessage);
        }

        public void Start()
        {
            TcpClient.Start();
        }
        public void Stop()
        {
            TcpClient.Close();
        }


        private void RouteTCPMessage(string message)
        {
            string target = message.Substring(1, message.IndexOf(']')-1);
            string data = message.Substring(message.IndexOf(']')+1);
            if (target == "ping")
            {
                CallbackMethodPing(ConvertToObject(data));
            }
        }
        public PingObject ConvertToObject(string jsonToPingObj)
        {
            return JsonConvert.DeserializeObject<PingObject>(jsonToPingObj);
        }

        public static string PingAsWindowsString(PingObject onePing)
        {
            if (onePing.pingTime != null)
            {
                return "Reply from " + onePing.hostIP + ": bytes=" + onePing.packetSize + " time=" + onePing.pingTime + "\n";
            }
            return "Request timed out.\n";
        }
    }

    public class Buffer<T>
    {
        private T[] ObjectsArray;
        private short OldestElement = -1;
        private short NewestElement = -1;
        private short _MaxElments = short.MaxValue;
        private short MaxElments
        {
            get
            {
                return _MaxElments;
            }
            set
            {
                if (value > short.MaxValue || value < 1) throw new ArgumentOutOfRangeException();
                _MaxElments = value;
            }
        }

        public Buffer(short maxElements)
        {
            MaxElments = maxElements;
            ObjectsArray = new T[MaxElments];
        }

        public void Add(T element)
        {
            if (NewestElement == -1 && OldestElement == -1)
            {
                NewestElement = 0;
                OldestElement = 0;
            }
            else if (NewestElement == 0 && OldestElement == 0)
            {
                NewestElement = 1;
                OldestElement = 0;
            }
            else
            {
                if (NewestElement >= MaxElments - 1) NewestElement = 0;
                else NewestElement++;
                if (NewestElement == OldestElement)
                {
                    if (OldestElement >= MaxElments - 1) OldestElement = 0;
                    else OldestElement++;
                }
            }
            ObjectsArray[NewestElement] = element;
        }

        public T Get(short index)
        {
            if (OldestElement == 0 && NewestElement < index) throw new IndexOutOfRangeException();
            int Index = index;
            Index += OldestElement;
            if (Index > MaxElments - 1) Index -= MaxElments;
            return ObjectsArray[Index];
        }

        public T this[short index]
        {
            get
            {
                return Get(index);
            }
            set
            {
                throw new InvalidOperationException();
            }
        }

        public T GetFirst()
        {
            return ObjectsArray[OldestElement];
        }
    }

    public struct PingObject
    {
        public string hostName;
        public string hostIP;
        public string pingTime;
        public int packetSize;
        public long packetNumber;
    }

    [Serializable]
    public class ConfigData
    {
        public string serverIP = "127.0.0.1";
        public int serverPort = 5000;
        public int clientID = 1234;
    }

    public class TcpConnection
    {
        private TcpClient TcpClientConnection;
        private NetworkStream TcpStream;
        private bool IsThreadAlive = true;
        private Action<string> CallbackMethod;
        private Thread ReceiveData;
        private string ServerIP;
        private int ServerPort;
        private int ClientID;
        public bool IsConnected = false;

        public TcpConnection(string serverIpAddress, int serverIpPort, int id, Action<string> callback)
        {
            ServerIP = serverIpAddress;
            ServerPort = serverIpPort;
            CallbackMethod = callback;
            ClientID = id;
        }

        public void Start()
        {
            try
            {
                TcpClientConnection = new TcpClient(ServerIP, ServerPort);
                ReceiveData = new Thread(ThreadReceiveData);
                TcpStream = TcpClientConnection.GetStream();
                IsThreadAlive = true;
                ReceiveData.Start();
                SendTCPData("[ID]" + ClientID);
                IsConnected = true;
            }
            catch (SocketException)
            {
                Debug.WriteLine("[ERROR] Socket Error");
                IsThreadAlive = false;
                IsThreadAlive = false;
                throw;
            }
            catch (Exception e)
            {
                Debug.WriteLine("[ERROR]" + e.GetType().ToString());
                Debug.WriteLine(e.Message.ToString());
                IsThreadAlive = false;
                IsConnected = false;
                throw;
            }
        }

        public void SendTCPData(string message)
        {
            if (TcpStream.CanWrite)
            {
                byte[] messageData = System.Text.Encoding.ASCII.GetBytes(message);
                TcpStream.Write(messageData, 0, messageData.Length);
                Debug.WriteLine("[INFO] Sending message");
            }
            else
            {
                Debug.WriteLine("[ERROR] Message not sended");
            }
        }

        private void ThreadReceiveData()
        {
            byte[] buffer;
            while (IsThreadAlive)
            {
                buffer = new Byte[512];
                try
                {
                    TcpStream.Read(buffer, 0, buffer.Length);
                    CallbackMethod(System.Text.Encoding.ASCII.GetString(buffer));
                }
                catch (System.IO.IOException)
                {
                    Debug.WriteLine("[ERROR] Error IO operation while waiting for TCP data.");
                    Thread.Sleep(200);
                    IsThreadAlive = false;
                    IsConnected = false;
                }
            }
        }

        public void Close()
        {
            if (IsThreadAlive)
            {
                IsThreadAlive = false;
                IsConnected = false;
                TcpClientConnection.Close();
                ReceiveData.Join();
            }
        }
    }
    class Configurator<T> where T : new()
    {
        string FilePath { get; set; }
        public T ActualConfiguration { get; set; }

        public Configurator(string filePath, ref T actualConfiguation)
        {
            FilePath = filePath;
            ActualConfiguration = actualConfiguation;
            LoadConfig();
            actualConfiguation = ActualConfiguration;
        }

        public void LoadConfig()
        {
            try
            {
                ActualConfiguration = ReadFromBinaryFile(FilePath);
            }
            catch (Exception e)
            {
                if (e is DecoderFallbackException)
                {
                    Debug.WriteLine("[ERROR] File \"" + FilePath + "\" is corrupted.");
                    File.Delete(FilePath);
                }
                else if (e is FileNotFoundException)
                {
                    Debug.WriteLine("[ERROR] No file  \"" + FilePath + "\" found.");
                }
                else
                {
                    Debug.WriteLine("[ERROR] " + e);
                    throw;
                }
                ActualConfiguration = new T();
            }
        }

        public void SaveConfig()
        {
            WriteToBinaryFile(FilePath, ActualConfiguration);
        }

        static void WriteToBinaryFile(string filePath, T objectToWrite, bool append = false)
        {
            using (Stream stream = File.Open(filePath, append ? FileMode.Append : FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, objectToWrite);
            }
        }

        static T ReadFromBinaryFile(string filePath)
        {
            using (Stream stream = File.Open(filePath, FileMode.Open))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                return (T)binaryFormatter.Deserialize(stream);
            }
        }
    }
}
