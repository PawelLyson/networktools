﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net.Sockets;
using System.Threading;

namespace PingClientTool
{
    public partial class MainGui : Form
    {
        private PingTool PingingServer;
        private bool isRunningAutomaticThread = false;
        private Thread RecconnectThread;

        public MainGui()
        {
            InitializeComponent();
        }

        private void LoadLogic_Shown(object sender, EventArgs e)
        {
            PingingServer = new PingTool(ReceivePingCallback);
            StartAutomaticRecconect();
        }

        private void StartAutomaticRecconect()
        {
            RecconnectThread = new Thread(AutomaticReconnectThread);
            RecconnectThread.Start();
            isRunningAutomaticThread = true;
        }
        private void StopAutomaticRecconect()
        {
            isRunningAutomaticThread = false;
            RecconnectThread.Join();
        }

        private void AutomaticReconnectThread()
        {
            while(isRunningAutomaticThread)
            {
                if (PingingServer.isConnected)
                {
                    for (int i=0; i<150 && isRunningAutomaticThread; i++) Thread.Sleep(10);
                }
                else
                {
                    ConnectToServer();
                }
                for (int i = 0; i < 500 && isRunningAutomaticThread; i++) Thread.Sleep(10);
            }
        }

        private void ConnectToServer()
        {
            try
            {
                PingingServer.Start();
                ConnectionStatusLabel.Invoke((MethodInvoker)delegate
                {
                    ConnectionStatusLabel.Text = "Połączono";
                });
            }
            catch (SocketException)
            {
                ConnectionStatusLabel.Invoke((MethodInvoker)delegate
                {
                    ConnectionStatusLabel.Text = "Brak połączenia z serwrem";
                });
            }
            catch (Exception error)
            {
                MessageBox.Show("Błąd łączenia:\n\n" + error.GetType().ToString() + "\n" + error.Message.ToString());
                ConnectionStatusLabel.Invoke((MethodInvoker)delegate
                {
                    ConnectionStatusLabel.Text = "Brak połączenia z serwrem";
                });
            }
        }

        private void ReceivePingCallback(PingObject pingObj)
        {
            AddTextToRichTextBox(PingTool.PingAsWindowsString(pingObj));
        }

        private void AddTextToRichTextBox(string text)
        {
            ForPingRichTextBox.Invoke((MethodInvoker) delegate
            {
                ForPingRichTextBox.Text += text;
                ForPingRichTextBox.SelectionStart = ForPingRichTextBox.Text.Length;
                ForPingRichTextBox.ScrollToCaret();
            });
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            StopAutomaticRecconect();
            PingingServer.Stop();
            base.OnFormClosing(e);
        }

        private void SettingsButton_Click(object sender, EventArgs e)
        {
            SettingsGui SettingForm = new SettingsGui();
            SettingForm.Show();
        }

        private void OnTopCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            TopMost = OnTopCheckBox.Checked;
        }
    }
}
