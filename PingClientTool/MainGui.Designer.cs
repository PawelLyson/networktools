﻿namespace PingClientTool
{
    partial class MainGui
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.ForPingRichTextBox = new System.Windows.Forms.RichTextBox();
            this.SettingsButton = new System.Windows.Forms.Button();
            this.OnTopCheckBox = new System.Windows.Forms.CheckBox();
            this.ConnectionStatusLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ForPingRichTextBox
            // 
            this.ForPingRichTextBox.Location = new System.Drawing.Point(12, 36);
            this.ForPingRichTextBox.Name = "ForPingRichTextBox";
            this.ForPingRichTextBox.Size = new System.Drawing.Size(532, 208);
            this.ForPingRichTextBox.TabIndex = 0;
            this.ForPingRichTextBox.Text = "";
            // 
            // SettingsButton
            // 
            this.SettingsButton.Location = new System.Drawing.Point(12, 7);
            this.SettingsButton.Name = "SettingsButton";
            this.SettingsButton.Size = new System.Drawing.Size(75, 23);
            this.SettingsButton.TabIndex = 1;
            this.SettingsButton.Text = "Ustawienia";
            this.SettingsButton.UseVisualStyleBackColor = true;
            this.SettingsButton.Click += new System.EventHandler(this.SettingsButton_Click);
            // 
            // OnTopCheckBox
            // 
            this.OnTopCheckBox.AutoSize = true;
            this.OnTopCheckBox.Location = new System.Drawing.Point(421, 11);
            this.OnTopCheckBox.Name = "OnTopCheckBox";
            this.OnTopCheckBox.Size = new System.Drawing.Size(123, 17);
            this.OnTopCheckBox.TabIndex = 2;
            this.OnTopCheckBox.Text = "Zawsze na wierzchu";
            this.OnTopCheckBox.UseVisualStyleBackColor = true;
            this.OnTopCheckBox.CheckedChanged += new System.EventHandler(this.OnTopCheckBox_CheckedChanged);
            // 
            // ConnectionStatusLabel
            // 
            this.ConnectionStatusLabel.Location = new System.Drawing.Point(407, 247);
            this.ConnectionStatusLabel.Name = "ConnectionStatusLabel";
            this.ConnectionStatusLabel.Size = new System.Drawing.Size(135, 14);
            this.ConnectionStatusLabel.TabIndex = 3;
            this.ConnectionStatusLabel.Text = "Brak połączenia z serwrem";
            this.ConnectionStatusLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // MainGui
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 269);
            this.Controls.Add(this.ConnectionStatusLabel);
            this.Controls.Add(this.OnTopCheckBox);
            this.Controls.Add(this.SettingsButton);
            this.Controls.Add(this.ForPingRichTextBox);
            this.Name = "MainGui";
            this.Text = "Form1";
            this.Shown += new System.EventHandler(this.LoadLogic_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox ForPingRichTextBox;
        private System.Windows.Forms.Button SettingsButton;
        private System.Windows.Forms.CheckBox OnTopCheckBox;
        private System.Windows.Forms.Label ConnectionStatusLabel;
    }
}

