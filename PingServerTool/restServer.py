import random
import string
import cherrypy
import globalOperations

@cherrypy.expose
class restWebServer(object):
    def __init__(self, callback):
        self.taskCallback = callback

    @cherrypy.tools.accept(media='text/plain')
    def GET(self):
        return cherrypy.session['mystring']

    def POST(self, length=8):
        some_string = ''.join(random.sample(string.hexdigits, int(length)))
        cherrypy.session['mystring'] = some_string
        return some_string

    def PUT(self, pingHostName, clientID):
        if globalOperations.IPOperations.ipv4isValid(pingHostName):
            self.taskCallback(pingHostName, clientID)
            cherrypy.response.status = 200
            return "OK"
        cherrypy.response.status = 406
        return "No Valid IP Address"
        #print(hostName)
        #cherrypy.session['mystring'] = another_string

    def DELETE(self):
        cherrypy.session.pop('mystring', None)


class WebServer(object):
    def __init__(self, callback):
        self.taskFunction = callback
        self.config = {
            '/': {
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                'tools.sessions.on': True,
                'tools.response_headers.on': True,
                'tools.response_headers.headers': [('Content-Type', 'text/plain')],
            }
        }
        cherrypy.tree.mount(restWebServer(self.taskFunction), '/', self.config)
        cherrypy.server.socket_host = "192.168.0.33"
        cherrypy.engine.start()

        #cherrypy.quickstart(restWebServer(self.pingFunction), '/', self.config)
