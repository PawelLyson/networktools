import pingLib
import time
import restServer
import tcpServer
import threading

#pingCollection.addNewPing("60.32.113.234", callback=print)
#pingCollection.addNewPing("8.8.8.8", callback=print)
#pingCollection.addNewPing("192.168.0.94", callback=print)
#time.sleep(10)
#time.sleep(10)


class PingServerTool(object):
    def __init__(self):
        self.tcpServerInstance = tcpServer.TCPServer(callback=print)
        self.restServerInstance = restServer.WebServer(self.prepareData)
        self.removeOutdatedPingThread = threading.Thread(target=self.removeOutdatedPing)
        self.isRunningRemovingOutdatedPing = False
        self.pingTargetList = []
        self.startRemovingOutdatePingThread()

    def prepareData(self, pingHostName, clientID):
        self.removeClientFromPing(clientID)
        self.createTaskPing(pingHostName, clientID)

    def startRemovingOutdatePingThread(self):
        self.isRunningRemovingOutdatedPing = True
        self.removeOutdatedPingThread.start()

    def stopRemovingOutdatePingThread(self):
        self.isRunningRemovingOutdatedPing = False
        self.removeOutdatedPingThread.join()

    def removeOutdatedPing(self):
        while self.isRunningRemovingOutdatedPing:
            for target in self.pingTargetList[:]:
                if target.isOutdated():
                    print("[INFO] Removing ping task: " + target.hostName)
                    target.stopPinging()
                    self.pingTargetList.remove(target)
            time.sleep(1)

    def removeClientFromPing(self, clientID):
        for target in self.pingTargetList:
            for connectedClient in target.connectedClients:
                if connectedClient == clientID:
                    target.removeClient(clientID)

    def createTaskPing(self, hostName, clientID):
        found = False
        for target in self.pingTargetList:
            if target.hostName == hostName:
                target.addClient(clientID)
                found = True
        if not found:
            self.pingTargetList.append(PingInfo(self.tcpServerInstance, hostName, clientID))

    def returnedPing(self, data):
        self.tcpServerInstance.sendToAll(data)

class PingInfo(object):
    def __init__(self, tpcInstance, hostName, addingClientID=None):
        self.hostName = hostName
        self.lastModified = time.time()
        self.pingCollection = pingLib.PingCollection()
        self.tcpServerInstance = tpcInstance
        self.connectedClients = []
        if addingClientID:
            self.addClient(addingClientID)
        self.pingCollection.addNewPing(hostName, callback=self.redirectDataFromPing)

    def stopPinging(self):
        self.pingCollection.stopAll()

    def isOutdated(self):
        if not self.connectedClients:
            return self.lastModified + 10 < time.time()
        else:
            self.updateTime()
            return False

    def addClient(self, clientID):
        self.updateTime()
        self.connectedClients.append(clientID)

    def getConnectedClients(self):
        return self.connectedClients

    def updateTime(self):
        self.lastModified = time.time()

    def removeClient(self, remove):
        self.connectedClients.remove(remove)

    def redirectDataFromPing(self, data, title):
        for clientID in self.connectedClients:
            self.tcpServerInstance.sendToClient(clientID, data, title)

PingServerTool()