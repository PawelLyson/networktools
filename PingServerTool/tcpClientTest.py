import socket
import threading

mySocket = socket.socket()

def Main():
    #host = '127.0.0.1'
    host = '192.168.1.156'
    port = 5000

    mySocket.connect((host, port))
    thread = threading.Thread(target=receiveData)
    thread.start()

    while True:
        message = input("->")
        mySocket.send(message.encode())


def receiveData():
    while True:
        #mySocket.send(message.encode())
        data = mySocket.recv(1024).decode()

        print('Received from server: ' + data)


    mySocket.close()


if __name__ == '__main__':
    Main()