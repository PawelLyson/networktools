import re

class IPOperations(object):
    @staticmethod
    def ipv4isValid(ipv4):
        ipv4_pattern = re.compile("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$")
        if ipv4_pattern.search(ipv4):
            return True
        return False