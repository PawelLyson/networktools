import pingLib.pingv4Lib
import threading
import time


def waitToTime(checkTime):
    while checkTime > time.time():
        time.sleep(0.01)


class PingCollection(object):
    def __init__(self):
        self.pingList = []

    def addNewPing(self, host, timeout=5000, timeBeetween=1000, packetSize=64, callback=None):
        pingTask = PingHost(host, timeout, timeBeetween, packetSize, callback)
        pingTask.start()
        self.pingList.append(pingTask)

    def getPingAddressList(self):
        addressList = []
        for onePing in self.pingList:
            addressList.append(onePing.hostName)
        return addressList

    def stopAll(self):
        for onePing in self.pingList:
            onePing.stop()

    def stopPinging(self, ipAddres):
        for onePing in self.pingList[:]:
            if onePing.hostIP == ipAddres:
                onePing.stop()
                self.pingList.remove(onePing)


class PingHost(object):
    def __init__(self, host, timeout=5000, timeBeetween=1000, packetSize=64, callback=None):
        self.stats = pingv4Lib.myStats()
        self.hostName = host
        self.hostIP = host #brak funkcji host to ip
        self.timeoutPing = timeout
        self.timeBetweenPings = timeBeetween / 1000
        self.packetSizePing = packetSize
        self.seqNumber = 0
        self.callbackFunction = callback
        self.isThreadRunning = False
        self.thread = threading.Thread(target=self.pingingThread)
        self.pingLibResult = None

    def start(self):
        self.isThreadRunning = True
        self.thread.start()

    def stop(self):
        print("[INFO] Stopping Ping " + self.hostIP)
        self.isThreadRunning = False
        self.thread.join()

    def pingingThread(self):
        while self.isThreadRunning:
            startTime = time.time()
            self.pingLibResult = pingv4Lib.do_one(self.stats, self.hostIP, self.hostName, self.timeoutPing, self.seqNumber, self.packetSizePing, True)
            self.seqNumber += 1
            self.sendCallback()
            waitToTime(startTime + self.timeBetweenPings)  # przy problemach z połąćzeniem metoda nie wykonuje się co skutkuje wyjątkiem w pętli(i zaprzestaniem jej działania)

    def sendCallback(self):
        if self.callbackFunction:
            result = {
                "hostName": self.hostName,
                "hostIP": self.hostIP,
                "pingTime": self.pingLibResult,
                "packetSize": self.packetSizePing,
                "packetNumber": self.seqNumber
            }
            self.callbackFunction(result, "ping")

    def getHostName(self):
        return self.hostName

    def getPacketReceived(self):
        return self.stats.pktsRcvd

    def getPacketSended(self):
        return self.stats.pktsSent

    def getLossPacket(self):
        return self.stats.pktsSent - self.stats.pktsRcvd

    def getPercentLossPacket(self):
        return self.getLossPacket() / self.getPacketSended()

    def getMinTimePacket(self):
        return self.stats.minTime

    def getMaxTimePacket(self):
        return self.stats.maxTime

    def getAverageTimePacket(self):
        return self.stats.totTime / self.stats.pktsSent


