import socket
import threading
import json

class TCPServer(object):
    def __init__(self, ip="192.168.1.156", port=5000, callback=None):
        self.callbackFunction = callback
        self.hostIP = ip
        self.hostPort = port
        self.mySocket = socket.socket()
        self.mySocket.bind((self.hostIP, self.hostPort))
        self.mySocket.listen(1)
        self.isListening = True
        self.clientList = []
        self.isListeningRunning = False
        self.thread = threading.Thread(target=self.listenningThread)
        self.thread.start()

    def listenningThread(self):
        while self.isListening:
            conn, addr = self.mySocket.accept()
            self.clientList.append(TCPHost(conn, addr, self.callbackFunction, self.cleanupClients))

    def sendToAll(self, data, title):
        self.cleanupClients()
        for client in self.clientList:
            client.sendData(data, title)

    def sendToClient(self, clientID, data, title):
        for client in self.clientList:
            if client.clientID == clientID:
                client.sendData(data, title)

    def cleanupClients(self):
        for client in self.clientList[:]:
            if not client.isAlive:
                self.clientList.remove(client)


class TCPHost(object):
    def __init__(self, conn, addr, callback, cleanCallback):
        self.callbackFunction = callback
        self.cleanFunctionCallback = cleanCallback
        self.isAlive = True
        self.clientID = None
        self.connection = conn
        self.address = addr
        self.isReceivingRunning = False
        self.receivingThread = threading.Thread(target=self.dataReceivingThread)
        self.startThread()
        print("[INFO] New TCP Connection")

    def __del__(self):
        self.connection.close()
        self.isReceivingRunning = False
        print("[INFO] Removing TCP Client " + self.clientID)

    def startThread(self):
        self.isReceivingRunning = True
        self.receivingThread.start()

    def getID(self):
        return self.clientID

    def sendData(self, data, title):
        try:
            self.connection.send(("[" + title + "]" + json.dumps(data, ensure_ascii=False)).encode())
        except OSError:
            print("[ERROR] Sending data error")
            print(OSError)
            self.closeTCPConnection()

    def dataReceivingThread(self):
        try:
            while self.isReceivingRunning:
                data = self.connection.recv(1024).decode()
                print("[INFO] Message: " + data)
                if not data:
                    break

                if data.find("[ID]", 0, 4) >= 0:
                    self.clientID = data[4:]
                    print("[INFO] Adding new TCP Client " + data[4:])
                    # self.callbackFunction(data[4:])
                # print("from connected  user: " + str(data))

                # self.sendData(data)
            self.connection.close()
        except Exception as error:
            print("[ERROR] " + str(error))
            self.closeTCPConnection()

    def closeTCPConnection(self):
        self.isReceivingRunning = False
        self.isAlive = False
        self.cleanFunctionCallback()





